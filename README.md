## Лицензия

Код доступен под лицензией [MIT](https://opensource.org/licenses/MIT).

Текстовая часть содержимого доступна под [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

## Сборка и деплой на itch.io

В системе должны быть установлены [node.js + npm](https://nodejs.org/download/) и [butler](https://itch.io/docs/butler/). В директории проекта необходимо выполнить команду `npm install`.

```bash
# все команды создают бинарники в папке dist/
npm run dist:win # создание windows-инсталлятора 
npm run dist:linux # создание deb-пакета (запускать только из под linux или WSL)
npm run dist:osx # создание macos-приложения
```

```bash
butler push ./src k12th/pillared:html5 # деплой новой онлайн-версии
butler push dist/pillared-1.0.0-win-x64-Setup.exe k12th/pillared:win # windows-инсталлятор
butler push dist/pillared-1.0.0-linux-x64 k12th/pillared:linux # архив для линукса
butler push dist/pillared-1.0.0-mac-x64 k12th/pillared:osx # архив для macos
```

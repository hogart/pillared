::Город-Колодец
<<set _caravanSerai = qbn.length('caravanSerai') >= 3 ? 'caravanserai' : 'massive two-storey building'>>\
<<if qbn.length('well') === 0>>\
  A lone round well sits on the square not far from [[_caravanSerai|Город-Караван-сарай]]. It is made of the same black stone; [[a long sweep|Город-Колодец-Журавль]] and a bucket hanging on it is made of old wood as dry as a bone.\
<<elseif qbn.has('well', 'crane')>>\
  <<if qbn.has('well', 'reject')>>\
	<<set _bucket = 'bucket of water'; _bucketLocation = 'nearby'; _linkTo = 'Город-Колодец-Вода'>>\
  <<elseif qbn.has('well', 'taste')>>\
	<<set _bucket = 'water'; _bucketLocation = 'nearby'; _linkTo = 'Город-Колодец-Вода'>>\
  <<elseif qbn.has('well', 'bucket')>>\
	<<set _bucket = 'water'; _bucketLocation = 'hanging on it'; _linkTo = 'Город-Колодец-Вода'>>\
  <<else>>\
    <<set _bucket = 'water'; _bucketLocation = 'hanging on it'; _linkTo = 'Город-Колодец-Ведро'>>\
  <</if>>\
  A lone round well sits on the square not far from [[_caravanSerai|Город-Караван-сарай]]. It is made of the same black stone; a long well sweep and <<= _bucketLocation>> [[_bucket|_linkTo]] — made of old withered but still intact wood.\
<</if>>\

From here, it is a stone’s throw to the [[entrance to the city|Город-Вход]].
<<include [[_well]]>>\

::Город-Колодец-Ведро
<<if !qbn.has('well', 'rope')>>\
  You droop the sweep to the well and hear a splash! However, there is no bubbling sound indicating that water is being poured into the bucket.
  You peep into the black aperture, straining to see something in the City’s grayish glow. Apparently, the rope is too short for the current water level. You need something to make it longer<<if $showHints>><<timed $hintsDelay t8n>>, for example, a piece of fabric<</timed>><</if>>... or untie it and try to reach water by hand, [[leaning over the ledge|Город-Колодец-Перегнуться]].
  But actually the well [[could wait|Город-Вход]] — you have enough water for now.
  <<include [[_inventory-hint]]>>
<<elseif !qbn.has('well', 'bucket')>>\
  You droop the sweep to the well and hear the bucket being filled with water. Carefully placing the bucket on the stones near the well, you scoop what’s inside with your hands and <<link 'examine' 'Город-Колодец-Ведро'>><<run qbn.set('well', 'water')>><</link>>.\
  <<run qbn.set('well', 'bucket')>>\
<<else>>\
  <<if qbn.has('well', 'taste')>>\
	The water from the well is clear and odourless. It is also deprived of some important properties that make water the elixir of life. You are not thirsty so you return to your [[exploration|Город-Колодец]].
  <<else>>\
	[[The water from the well|Город-Колодец-Вода]] is clear and odourless.
  <</if>>\
<</if>>\
<<include [[_well]]>>\

::Город-Колодец-Вода
<<if qbn.has('well', 'taste')>>\
  Your survival may hinge on whether this water is potable. But you are not [[thirsty|Город-Колодец-Попробовать]] at all and the total lack of odour somehow confuses you. Maybe just [[splash it out|Город-Колодец-Выплеснуть]]?\
<<elseif qbn.has('well', 'reject')>>\
  Your survival may hinge on whether this water is potable. But you are not thirsty at all and the total lack of odour somehow confuses you.[[Splash Out|Город-Колодец-Выплеснуть]] or [[try it|Город-Колодец-Попробовать]]?\
<<else>>\
  Looking at this clear, pleasantly cold, odourless water, you realise that you are <<more "but in the morning you would probably kill for a gulp of it">>not thirsty<</more>>. Nevertheless, being able to drink this water may save your life. Only if the appearance of its clarity guaranteed that you would not be poisoned... [[Splash it out|Город-Колодец-Выплеснуть]] or [[try it|Город-Колодец-Попробовать]]?\
<</if>>\
<<include [[_well]]>>\

::Город-Колодец-Журавль
<<if !qbn.has('well', 'crane')>>\
  You carefully touch the sweep, expecting it to fall apart and turn into dust and wood chips, but to your great surprise the wood is durable as if it were no more than ten years old. Maybe those nomads in red garments use it and replace it every once in a while? For want of other ideas, you accept it as a working hypothesis.
  And this means two things.
  First, there may be someone else in the city. You quickly check your revolver and promise yourself to be on your guard.
  Second, if the well is being used, there must be [[water|Город-Колодец-Ведро]] in it.
  <<run qbn.set('well', 'crane')>>
<<else>>
  The wood the sweep and the [[bucket|Город-Колодец-Ведро]] are made of looks withered but is quite durable.
<</if>>\
<<include [[_well]]>>\

::Город-Колодец-Выплеснуть
After all, the well is here to stay. You splash the water out on the ground and numbly watch the water trickle down the small grooves of the carvings, rendering them darker. Yes, even the stone plates under your feet are interspersed with images of this eerie culture, architects of this eerie city. Here you can see, as archaeologists say, stylised images of people or strange creatures with long limbs and large heads. Some look more like monsters depicted as chimeras: parts of animals are depicted instead of human heads and limbs such as aurochs or crocodiles; some are even beyond recognition as the drawing is too schematic. All of them fight, perform some rituals, devour each other. This is simply a treasure for anthropologists! The intricate carvings are attractive and [[repulsive|Город-Колодец]] at the same time.
<<run qbn.set('well', 'reject');>>\
<<include [[_well]]>>\

::Город-Колодец-Попробовать
<<set _abbr = $isFemale ? "When you were a student thirsting for knowledge, you once tasted distilled water despite warnings from your friends" : "When you were a sociable student, you once tasted distilled water out of bravery despite warnings from your colleagues">>\
It’s ordinary water by taste, but it contains <<more _abbr>>something<</more>> because of which [[you don’t feel like|Город-Колодец]] taking another sip.
<<run qbn.set('well', 'taste'); if (!qbn.has('well', 'taste')) { qbn.inc('madness', 20); }>>\
<<include [[_well]]>>\

::Город-Колодец-Перегнуться [death]
Unfortunately, you overestimated your physical fitness. A momentary dizziness and... 
The well’s walls are as smooth as glass and it is impossible to climb them, especially with wet hands and clothes.

Giving up is not your style and for some time you try to call <<more "even the red Bedouins could pull you out — at least so that a foreigner does not desecrates their well">>for help<</more>>.
<<timed 3s t8n>>But staying afloat forever is beyond human power.
----
<<include [[_you-died]]>><</timed>>

::_well
<<if qbn.length('well') >= 3>>
  <<run qbn.set('majorCity', 'well')>>
<</if>>
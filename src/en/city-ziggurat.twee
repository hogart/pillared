::Город-Зиккурат
<<if qbn.has('ziggurat', 'foot')>>\
  You are on a huge square with a [[ziggurat|Город-Зиккурат-Верх]] at its centre. \
<<else>>\
  The street suddenly leads you to a huge square with a ziggurat at its centre. \
<</if>>\
Between eighty or hundred feet tall, made of the same black stone, with the same inconceivable precision, it represents something between step pyramids of the New World and Mesopotamia’s ziggurats, the only difference being that <<more "the colour of the city’s pavement">>white<</more>> columns stand in the corners of each step. They converge at a roofless square portal at the top. Their surface is speckled with carvings — the same odd stylized chimeras and <<more "someone is going to make quite a name for himself as a new Champollion">>pictograms<</more>>.
<<if !qbn.has('ziggurat', 'foot')>>As the ziggurate is in excellent condition, you can [[climb it|Город-Зиккурат-Верх]].<</if>>
You can clearly imagine [[the way back|Город-Вход]].
<<run qbn.set('ziggurat', 'foot')>>

::Город-Зиккурат-Верх
<<if !qbn.has('ziggurat', 'altar') && qbn.length('cityPlan') < 2>>\
  You walk up large steps, and each of your steps requires <<more "however, you feel no fatigue or thirst">>some effort<</more>>.
<</if>>\
The ziggurat’s top is practically a flat surface — even the floor is devoid of the ubiquitous carvings, \
<<if !qbn.has('ziggurat', 'altar')>>\
  though there is a stone waist-high dais in the middle, apparently a [[sacrificial altar|Город-Зиккурат-Алтарь]].\
<<else>>\
  — except for the [[altar|Город-Зиккурат-Алтарь]] in the middle.\
<</if>>

A splendid view of the city opens from the top – \
<<if qbn.length('cityPlan') < 2>>\
  you can finally [[get your bearings|Город-Планировка]].\
<<else>>\
  you can refresh [[the city plan|Город-Планировка]].\
<</if>>


::Город-Планировка
<<if qbn.length('cityPlan') < 2>>\
  <<run qbn.set('cityPlan', 'common')>>\
  Looking from the top, what you have already suspected roaming the crooked streets becomes obvious: the city plan is unlike anything you’ve seen. The human history has known two types of cities: with streets following the chaotic whim of the terrain or with a geometric rectangular layout. <<include [[_планировка]]>>
  <<if !qbn.has('cityPlan', '8')>>\
  	<<linkreplace "You look back...">>
        <<run qbn.set('cityPlan', '8')>>\
        And there is something else. The city fits into two circles rather than one, forming a cyclopean “8” of sorts. The smaller part represents the same crazy whirlwind of streets with the same square and a monumental structure in the middle. The architecture in this part is somewhat different, but you need to move closer to see the details.\
    <</linkreplace>>\
  <<else>>\
    <<include [[_планировка8]]>>
  <</if>>\
<<else>>\
  <<include [[_планировка]]>>\
  But you are a scientist. You’ve got to go, take the city’s secrets by force and, if possible, bring them back home.
  <<include [[_планировка8]]>>
<</if>>
<<if qbn.has('ziggurat', 'altar')>>
  You are thinking whether to walk [[down|Город-Зиккурат]] or [[examine the altar|Город-Зиккурат-Алтарь]] once again.
<<else>>
  [[The further way|Город-Зиккурат]] is clear, but first it is worth examining [[the pyramid’s top|Город-Зиккурат-Верх]].
<</if>>

::_планировка
Local streets are a bunch of circles without a common centre. There are no wide radial roads here (as one could expect); short chords and curves form meanders and bits of multiple spirals, webs of winding lightnings; as if someone threw a handful of rocks into turbulent waters; nonetheless, this drawing has some <<= either('terrifying', 'alien')>> beauty of its own, akin to a rhythm of beautiful nonhuman music.

The locals apparently knew how to get from their home to a desired location, but your eyes are dazzled by this wild plexus. The idea that you’ll have to walk down and roam through this maze gives you an image of a bug lost on a page of a mathematical treatise. Probably — a displeasing idea flashes through your mind — you understand this city as much as this bug understands Mr. Lobachevski’s works. The ink of the buildings will engulf you as a stormy sea engulfs a pebble.

::_планировка8
<<linkreplace "You look back...">>\
The unexplored part of the gigantic eight draws your eye. It stops at a remote square; it seems that the black building in its centre is slightly vibrating. The hot air must be playing with optical rays; but the sun is covered by clouds...
<</linkreplace>>

::Город-Зиккурат-Алтарь
It’s hard to say what gods this city’s inhabitants were worshiping. But they apparently offered human sacrifices — a massive altar made of white stone has the shape of a man with his limbs stretched out. The bulges for arms and legs have holes for belts, the incredibly hard stone is smoothed out by a huge number of death throes.
The stone surface is covered with <<more "rather fine and precise work">>hundreds of grooves<</more>> converging into several little canals leading inside the ziggurat.

You could only be grimly happy that you found yourself in this city after its inhabitants had disappeared — <<more "or whoever?..">>whatever<</more>> had wiped them off the face of the earth. They would not have appreciated your curiosity and you would surely have disliked their... hospitality.

<<if qbn.has('cityPlan')>>\
You [[turn away|Город-Зиккурат-Вверх]].\
<<else>>\
You [[stop|Город-Планировка]] to look around.\
<</if>>
<<run qbn.set('ziggurat', 'altar');>>
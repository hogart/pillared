window.visitedInventory = function () {
    return visitedTags('inventory');
};


preloadImages([
    'title-day.png',
    'title-night.png',

    'bg/bg-city-day.jpg',
    'bg/bg-city-night.jpg',
    'bg/bg-desert-day.jpg',
    'bg/bg-desert-night.jpg',
    'bg/bg-sea-day.jpg',
    'bg/bg-sea-night.jpg',

    'bg/fg-city-day.png',
    'bg/fg-city-night.png',
    'bg/fg-desert-day.png',
    'bg/fg-desert-night.png',
    'bg/fg-sea-day.png',
    'bg/fg-sea-night.png',
].map(src => `./img/${src}`));
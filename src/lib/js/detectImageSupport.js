(function detectImageSupport() {
    // borrowed from https://github.com/leechy/imgsupport/blob/master/imgsupport.js
    'use strict';

    const docEl = document.documentElement;

    function setHTMLClass(height, className) {
        if (height === 2) {
            docEl.classList.add('supports-' + className);
        } else {
            docEl.classList.add('no-' + className);
        }
    }

    var AVIF = new Image();
    AVIF.onload = AVIF.onerror = function () {
        setHTMLClass(AVIF.height, 'avif');
    }
    AVIF.src = 'data:image/avif;base64,AAAAIGZ0eXBhdmlmAAAAAGF2aWZtaWYxbWlhZk1BMUIAAADybWV0YQAAAAAAAAAoaGRscgAAAAAAAAAAcGljdAAAAAAAAAAAAAAAAGxpYmF2aWYAAAAADnBpdG0AAAAAAAEAAAAeaWxvYwAAAABEAAABAAEAAAABAAABGgAAAB0AAAAoaWluZgAAAAAAAQAAABppbmZlAgAAAAABAABhdjAxQ29sb3IAAAAAamlwcnAAAABLaXBjbwAAABRpc3BlAAAAAAAAAAIAAAACAAAAEHBpeGkAAAAAAwgICAAAAAxhdjFDgQ0MAAAAABNjb2xybmNseAACAAIAAYAAAAAXaXBtYQAAAAAAAAABAAEEAQKDBAAAACVtZGF0EgAKCBgANogQEAwgMg8f8D///8WfhwB8+ErK42A=';

    var WebP = new Image();
    WebP.onload = WebP.onerror = function () {
        setHTMLClass(WebP.height, 'webp');
    };
    WebP.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
})();